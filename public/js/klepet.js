var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  //console.log('KANAL: ' + kanal);
  var kanalFix = kanal.split('@ ')[1];
  //console.log('KANALfix: ' + kanalFix);
  var sporocilo = {
    kanal: kanalFix,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

Klepet.prototype.spremeniKanalzGeslom = function(kanal,geslo) {
  this.socket.emit('pridruzitevZahtevaZGeslom', {
    novKanal: kanal,
    geslo: geslo
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  var celotenUkaz = ukaz;
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var preostanekBesede = besede.join(' ');
      var kanal;
      var geslo;
      if (preostanekBesede.charAt(0) == '"') {
        kanal = preostanekBesede.substring(1,preostanekBesede.length);
        var index = kanal.indexOf('"');
        if (index < 0) {
          sporocilo = 'Neznan ukaz.';
          break;
        }
        kanal = kanal.substring(0,index);
        console.log('KANAL: ' + kanal);
        preostanekBesede = preostanekBesede.substring(index+2,preostanekBesede.length);
        index = preostanekBesede.indexOf('"');
        if (index < 0) {
          sporocilo = 'Neznan ukaz.';
          break;
        }
        geslo = preostanekBesede.substring(index+1,preostanekBesede.length-1);
        console.log('GESLO: ' + geslo);
        this.spremeniKanalzGeslom(kanal,geslo);
      }
      else {
        kanal = preostanekBesede;
        this.spremeniKanal(kanal);
      }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var preostanekBesede = besede.join(' ');
      var naslovnik = preostanekBesede.substring(1,preostanekBesede.length);
      var index = naslovnik.indexOf('"');
      if (index < 0) {
        sporocilo = 'Neznan ukaz.';
        break;
      }
      naslovnik = naslovnik.substring(0,index);
      console.log(naslovnik);
      preostanekBesede = preostanekBesede.substring(index+2,preostanekBesede.length);
      index = preostanekBesede.indexOf('"');
      if (index < 0) {
        sporocilo = 'Neznan ukaz.';
        break;
      }
      var sporociloTmp = preostanekBesede.substring(index+1,preostanekBesede.length-1);
      console.log(sporociloTmp);
      
      this.socket.emit('zasebnoSporocilo', {naslovnik:naslovnik, besedilo:sporociloTmp});
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};