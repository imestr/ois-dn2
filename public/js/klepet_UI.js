function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    
    var besede = sporocilo.split(' ');
    for (var i = 0; i < besede.length; i++) {
      var beseda = besede[i];
      var firstChar =beseda.charAt(0);
      if (firstChar == '.' || firstChar == ',' || firstChar == ';' || firstChar == '!' || firstChar == '?' || firstChar == ':') {
        beseda = beseda.substring(1,beseda.length);
      }
      
      var lastChar =beseda.charAt(beseda.length-1);
      //console.log(lastChar);
      if (lastChar == '.' || lastChar == ',' || lastChar == ';' || lastChar == '!' || lastChar == '?' || lastChar == ':') {
        beseda = beseda.substring(0,beseda.length-1);
      }
      
      for (var j = 0; j < words.length; j++) {
        if (beseda.toLowerCase() == words[j].childNodes[0].nodeValue) {
          var tmpBeseda = '';
          if (firstChar == '.' || firstChar == ',' || firstChar == ';' || firstChar == '!' || firstChar == '?' || firstChar == ':') {
            tmpBeseda += besede[i].charAt(0);
          }
          for (var k = 0; k < beseda.length; k++) {
            tmpBeseda += '*';
          }
          if (lastChar == '.' || lastChar == ',' || lastChar == ';' || lastChar == '!' || lastChar == '?' || lastChar == ':') {
            tmpBeseda += besede[i].charAt(besede[i].length-1);
          }
          besede[i] = tmpBeseda;
          break;
        }
      }
    }
    sporocilo = besede.join(' ');
    
    var sporociloTmp2 = sporocilo;
    while (true) {
      var sporociloTmp = '';
      var index = sporocilo.indexOf(';)');
      if(index > -1) {
        sporociloTmp = sporocilo.substring(0,index);
        sporociloTmp += '<img src = \"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png\">';
        sporociloTmp += sporocilo.substring(index+2,sporocilo.length);
        sporocilo = sporociloTmp;
      }
      index = sporocilo.indexOf(':)');
      if(index > -1) {
          sporociloTmp = sporocilo.substring(0,index);
        sporociloTmp += '<img src = \"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png\">';
        sporociloTmp += sporocilo.substring(index+2,sporocilo.length);
        sporocilo = sporociloTmp;
      }
      index = sporocilo.indexOf('(y)');
      if(index > -1) {
          sporociloTmp = sporocilo.substring(0,index);
        sporociloTmp += '<img src = \"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png\">';
        sporociloTmp += sporocilo.substring(index+3,sporocilo.length);
        sporocilo = sporociloTmp;
      }
      index = sporocilo.indexOf(':*');
      if(index > -1) {
          sporociloTmp = sporocilo.substring(0,index);
        sporociloTmp += '<img src = \"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png\">';
        sporociloTmp += sporocilo.substring(index+2,sporocilo.length);
        sporocilo = sporociloTmp;
      }
      index = sporocilo.indexOf(':(');
      if(index > -1) {
        sporociloTmp = sporocilo.substring(0,index);
        sporociloTmp += '<img src = \"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png\">';
        sporociloTmp += sporocilo.substring(index+2,sporocilo.length);
        sporocilo = sporociloTmp;
      }
      if(sporociloTmp === '') {
        break;
      }
    }
    
    klepetApp.posljiSporocilo($('#kanal').text(), sporociloTmp2);
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }
  $('#poslji-sporocilo').val('');
}

function loadXMLDoc(filename) {
  var xhttp;
  if (window.XMLHttpRequest)
    {
    xhttp=new XMLHttpRequest();
    }
  else // code for IE5 and IE6
    {
    xhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
  xhttp.open("GET",filename,false);
  xhttp.send();
  return xhttp.responseXML;
}

var socket = io.connect();
var words;

$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  var vzdevek,kanal;

  var xmlDoc=loadXMLDoc("/swearWords.xml");
  words = xmlDoc.getElementsByTagName("word");
  //console.log(words[2].childNodes[0].nodeValue);
  
  //console.log($(window).width()+"x "+$(window).height());
  
  $('#vsebina').css('width',$(window).width()-100);
  $('#sporocila').css('width',(($(window).width()-100)*0.85)-10);
  $('#seznam').css('width',($(window).width()-100)*0.15);
  
  
  $(window).resize(function(){
    $('#vsebina').css('width',$(window).width()-100);
    $('#sporocila').css('width',(($(window).width()-100)*0.85)-10);
    $('#seznam').css('width',($(window).width()-100)*0.15);
  });

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
    $('#kanal').text(rezultat.vzdevek+' @ '+kanal);
    vzdevek = rezultat.vzdevek;
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal').text(vzdevek+' @ '+rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
    kanal = rezultat.kanal;
  });

  socket.on('sporocilo', function (sporocilo) {
    var sporocilo1 = sporocilo.besedilo;
    //var sporociloTmp2 = sporocilo.besedilo;
    
    var besede = sporocilo1.split(' ');
    for (var i = 0; i < besede.length; i++) {
      var beseda = besede[i];
      var firstChar =beseda.charAt(0);
      if (firstChar == '.' || firstChar == ',' || firstChar == ';' || firstChar == '!' || firstChar == '?' || firstChar == ':') {
        beseda = beseda.substring(1,beseda.length);
      }
      
      var lastChar =beseda.charAt(beseda.length-1);
      //console.log(lastChar);
      if (lastChar == '.' || lastChar == ',' || lastChar == ';' || lastChar == '!' || lastChar == '?' || lastChar == ':') {
        beseda = beseda.substring(0,beseda.length-1);
      }
      
      for (var j = 0; j < words.length; j++) {
        if (beseda.toLowerCase() == words[j].childNodes[0].nodeValue) {
          var tmpBeseda = '';
          if (firstChar == '.' || firstChar == ',' || firstChar == ';' || firstChar == '!' || firstChar == '?' || firstChar == ':') {
            tmpBeseda += besede[i].charAt(0);
          }
          for (var k = 0; k < beseda.length; k++) {
            tmpBeseda += '*';
          }
          if (lastChar == '.' || lastChar == ',' || lastChar == ';' || lastChar == '!' || lastChar == '?' || lastChar == ':') {
            tmpBeseda += besede[i].charAt(besede[i].length-1);
          }
          besede[i] = tmpBeseda;
          break;
        }
      }
    }
    sporocilo1 = besede.join(' ');
    
    while (true) {
      var sporociloTmp = '';
      var index = sporocilo1.indexOf(';)');
      if(index > -1) {
        sporociloTmp = sporocilo1.substring(0,index);
        sporociloTmp += '<img src = \"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png\">';
        sporociloTmp += sporocilo1.substring(index+2,sporocilo1.length);
        sporocilo1 = sporociloTmp;
      }
      index = sporocilo1.indexOf(':)');
      if(index > -1) {
          sporociloTmp = sporocilo1.substring(0,index);
        sporociloTmp += '<img src = \"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png\">';
        sporociloTmp += sporocilo1.substring(index+2,sporocilo1.length);
        sporocilo1 = sporociloTmp;
      }
      index = sporocilo1.indexOf('(y)');
      if(index > -1) {
          sporociloTmp = sporocilo1.substring(0,index);
        sporociloTmp += '<img src = \"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png\">';
        sporociloTmp += sporocilo1.substring(index+3,sporocilo1.length);
        sporocilo1 = sporociloTmp;
      }
      index = sporocilo1.indexOf(':*');
      if(index > -1) {
          sporociloTmp = sporocilo1.substring(0,index);
        sporociloTmp += '<img src = \"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png\">';
        sporociloTmp += sporocilo1.substring(index+2,sporocilo1.length);
        sporocilo1 = sporociloTmp;
      }
      index = sporocilo1.indexOf(':(');
      if(index > -1) {
        sporociloTmp = sporocilo1.substring(0,index);
        sporociloTmp += '<img src = \"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png\">';
        sporociloTmp += sporocilo1.substring(index+2,sporocilo1.length);
        sporocilo1 = sporociloTmp;
      }
      if(sporociloTmp === '') {
        break;
      }
    }
    
    $('#sporocila').append(divElementHtmlTekst(sporocilo1));
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  socket.on('osveziUporabnike', function(uporabljeniVzdevki) {
    $('#seznam-uporabnikov').empty();
    
    //console.log(uporabljeniVzdevki);
    for(var i = 0; i < uporabljeniVzdevki.length; i++) {
      console.log(uporabnik);
      var uporabnik = uporabljeniVzdevki[i];
      //uporabnik = uporabnik.substring(0, uporabnik.length);
      if (uporabnik != null) {
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabnik));
      }
    }
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});